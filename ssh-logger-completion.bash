#!/usr/bin/env bash

_ssh_logger_completions()
{   
    if [ "${#COMP_WORDS[@]}" != "2" ]; then
	return
    fi

    # check if @ in argument
    # if true remove everything before the "@" to only keep the servername
    if [[ $COMP_WORDS[1] == *"@"* ]]; then
	COMP_WORDS[1]=$(echo $COMP_WORDS[1] | cut -d "@" -f 2)
    fi

    COMPREPLY=($(compgen -W "$(generate_wordlist)" "${COMP_WORDS[1]}"))
} 

generate_wordlist()
{
    known_hosts=`cat $HOME/.ssh/known_hosts | awk '{ print $1 }' | uniq`
    echo $known_hosts
}

complete -F _ssh_logger_completions ssh-logger

#known_hosts=`cat $HOME/.ssh/known_hosts | awk '{ print $1 }' | uniq`
#echo $known_hosts
