# SSH Logger
This scripts provides tab-completion for hostnames based on your known-hosts file.
Additionally it will log all you ssh-sessions it `~/ssh-logs/`.

## Installation
Simply copy of link the files into you path and make the executable.
```
ln -s ssh-logger /usr/local/bin/ssh-logger
ln -s ssh-logger-completion.bash /usr/local/bin/ssh-logger-completion.bash

chmod +x /usr/local/bin/ssh-logger
chmod +x /usr/local/bin/ssh-logger-completion.bash
```

For the tab-completion to work you need to source the completion script in you `.bashrc`.
```
echo "source /usr/local/bin/ssh-logger-completion.bash >> ~/.bashrc
```

If you want you can alias ssh to ssh-logger.
```
echo "alias ssh=ssh-logger" >> ~/.bashrc
```

## Usage
ssh-logger has the same syntax as ssh. You can add more arguments and they will get passed to your ssh-session.
```
ssh-logger myserver -l myuser
ssh-logger myserver -l myuser -X
ssh-logger myuser@myserver
ssh-logger myuser@myserver -X
```

## Reading the Logs
Since the logs contain the plain bash-output, they also contain escape-squences. Because of that most text-editors will not display them correctly.
You can use less with the -r argument to read the logs correctly.
```
less -r ~/ssh-logs/session.log
```
